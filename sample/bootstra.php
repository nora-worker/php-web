<?php
require_once realpath(__DIR__.'/..').'/vendor/autoload.php';

# Noraの初期化
Nora::initialize(__DIR__, 'devel');

# Webモジュールの初期化
Nora::ModuleLoader( )
    ->load('Web');


