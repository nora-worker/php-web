<?php
/**
 * ウェブモジュールのサンプル
 */
namespace Sample\Web;

use Nora\Module\Web\Controller\Controller;

class Index extends Controller
{
    /**
     * Index Action
     *
     * @name index
     * @route /
     * @route /index
     * @route /index.html
     * @route /index.php
     */
    public function index ( )
    {
    }
}

