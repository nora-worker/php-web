<?php
/**
 * ウェブモジュールのサンプル
 */
require_once realpath(__DIR__.'/..').'/vendor/autoload.php';

# Noraの初期化
Nora::initialize(__DIR__, 'devel');

# Webモジュールを有効にする
Nora::ModuleLoader( )->load('Web');

# オートローダ
Nora::Autoloader( )->addLibrary(
    __DIR__.'/web',
    'Sample\Web'
);

# WEB用の設定
Nora::Config( )->set('web', [
    'ns' => 'Sample\Web'
]);


#
# ルータを登録
# / | index
Nora::Web( )->addController(
    'Index'
)->run( );
