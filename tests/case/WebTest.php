<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Web;

use Nora;

/**
 * WEBのテスト
 *
 */
class WebTest extends \PHPUnit_Framework_TestCase
{
    public function testWeb ( )
    {
        // 開発用
        Nora::module('logging', 'web');

        // テスト用に環境変数を用意する
        Nora::environment_server( )->set([
            'REQUEST_URI'    => '/user/kurari',
            'REQUEST_METHOD' => 'GET'
        ]);

        Nora::AutoLoader_addLibrary([
            'Sample\Web' => TEST_PROJECT_PATH.'/sample/web'
        ]);

        // フロントコントローラを取得
        $front = Nora::Web_Front();

        Nora::logging_start()->addLogger([
            'writer' => 'stdout'
        ]);


        $front->addController('Sample\Web\Home');

        $front->run();
        //$front->run();


        /**


        Nora::Web( )
            ->addController(
                'Sample\Web\Home'
            )->run();

        # 単体の実行
        $ctl = new \Sample\Web\Home($web2 = Nora::Web()->scope());
        $ctl->execute('index');
        $web2->response()->send();
        **/
    }
}
