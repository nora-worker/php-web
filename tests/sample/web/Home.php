<?php
/**
 * ウェブモジュールのサンプル
 */
namespace Sample\Web;

use Nora\Module\Web\Controller\Controller;

class Home extends Controller
{
    /**
     * Index Action
     *
     * @route /
     * @route /index
     * @route /user/{name}
     * @route /index.html
     * @route /index.php
     * @inject request
     * @inject response
     */
    public function index ($req, $res)
    {
        $name = $req->get('name', 'no-name');

        $res->write('hello world from '.$name);
    }
}

