<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Web;

use Nora\Core\Component;

/**
 * WEB フロントコントローラ
 */
class Front 
{
    use Component\Componentable;

    private $_routes;

    protected function initComponentImpl( )
    {
        // スコープとして登録
        $this->setComponent('@Front', $this);

        // 404時のエラー関数を登録
        $this->notfound(function ( ) {
            $res = $this->response()->clear()->status(404);
            $res->write('<h1>404 not found</h1>');
            $res->write('<blockquote>');
            $res->write('URL: '.$this->request()->getMethod().' '.$this->request()->getUri());
            $res->write('</blockquote>');
            $res->send();
        });
    }

    public function setBaseUrl($url)
    {
        $this->getScope()->baseurl = $url;
        return $this;
    }

    public function getBaseUrl( )
    {
        return $this->getScope()->baseurl;
    }

    /**
     * Web Controllerを登録する
     */
    public function addController ($class, $path = null)
    {
        $this->logDebug("controller added ".$class.($path === null ? '': ' on '.$path));
        $this->router()->addController($class, $path, $log);
        return $this;
    }

    /**
     * Not Found時の処理
     */
    public function notfound ($cb)
    {
        $this->setHelper('on404', $cb);
        return $this;
    }

    /**
     * Redirectの処理
     */
    public function redirect($path, $datas = [], $method = 'GET')
    {
        // 名前付きリダイレクト
        if ($path{0} === '@')
        {
            $mark = $path;

            $path=$this->router()->getNamePath(substr($path, 1), $method);
            $this->debug(sprintf(
                'redirect mark %s => %s',
                $mark, $path
            ));
            $this->redirect($path, null, $method);
        }

        if (empty($datas))
        {
            // リダイレクトの実行
            $this->response()
                ->clear()
                ->status(303)
                ->header('Location', $path)
                ->send();
        }else{
            die('未実装');
        }

    }

    public function route ($name, $cb)
    {
        $this->router( )->map($name, $cb);
        return $this;
    }

    /**
     * リクエストオブジェクトを取得する
     */
    public function bootRequest ( )
    {
        $request = new Request( );
        $request->setScope($this->newScope());
        return $request;
    }

    /**
     * ルートオブジェクト
     */
    public function bootRouter( )
    {
        $router =  new Router( );
        $router->setScope($this->newScope());
        return $router;
    }

    /**
     * レスポンスオブジェクト
     */
    public function bootResponse( )
    {
        $response =  new Response( );
        $response->setScope($this->newScope());
        return $response;
    }


    public function run ( )
    {
        $dispatched = false;
        $req = $this->request();

        // ログをとっておく
        $this->logInfo([
            'Accepted Request' => $req->getMethod().' '.$req->getUri(),
            'HTTP_HOST' => $this->getEnv('HTTP_HOST')
        ]);

        while($route = $this->router()->route($req->getUri(), $req->getMethod()))
        {
            // Placeholder
            $placeholder = $route['placeholder'];

            // スコープを作成
            // 新しいリクエストクラス
            $newScope = $this->newScope()->setComponent([
                'request' => $req->newRequest($placeholder)
            ]);

            // Specを取得
            $type = $route['spec'][0];

            if ($type === 'bind')
            {
                // 対象クラス
                $class = $route['spec'][1]['class'];
                // 依存リスト
                $injections = $route['spec'][1]['injections'];
                // 対象メソッド
                $method = $route['spec'][1]['method'];

                // ルーターに依って付与されたURL
                $mask = $route['spec'][1]['mask'];

                // マスクをセットする
                $newScope->request()->setMask($mask);

                $this->logDebug([
                    'run-class' => $class,
                    'run-method' => $method
                ]);

                $closure = $class::buildClosure($newScope, $method, $injections);
            }else{
                $closure = $route['spec'][0];
            }

            if (false !== call_user_func($closure))
            {
                $dispatched = true;
                break;
            }

            $this->router( )->next();
        }

        if ($dispatched === false)
        {
            $this->logInfo($req->getMethod().' '.$req->getUri().' Not Found');
            $this->on404();
        }

        // 終了処理
        $this->response()->send();
    }
}
