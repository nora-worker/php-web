<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Web;

use Nora\Core\Component;

use Nora\Module\Environment\Environment;
use Nora\Core\Util\Collection\Hash;

/**
 * WEB用のREQUEST
 */
class Request
{
    use Component\Componentable;

    private $_env;
    private $_datas = [];
    private $_mask;

    public function initComponentImpl( )
    {
    }

    public function setMask($mask)
    {
        $this->_mask = $mask;
        return $this;
    }

    public function genUrl($path)
    {
        return $this->_mask.'/'.$path;
    }

    /**
     * URIを取得する
     *
     * @return string
     */
    public function getUri ( )
    {
        $uri = $this->getEnv('REQUEST_URI');

        if (false !== $p = strpos($uri, $this->Front()->getBaseUrl())) {
            $uri = substr($uri, strlen($this->Front()->getBaseUrl()));
        }

        if (empty($uri)) return '/';
        return $uri;
    }

    /**
     * METHODを取得する
     *
     * @return string
     */
    public function getMethod ( )
    {
        $method = $this->getEnv('REQUEST_METHOD');

        if (empty($method)) return 'GET';
        return $method;
    }

    public function &get ($name, $default = null)
    {
        if(isset($this->_datas[$name]))
        {
            return $this->_datas[$name];
        }

        if ($this->environment_POST( )->has($name))
        {
            return $this->environment_POST()->get($name);
        }

        if ($this->environment_GET( )->has($name))
        {
            return $this->environment_GET()->get($name);
        }

        $ret = $default;

        return $ret;
    }


    public function newRequest ($vars)
    {
        $req = clone $this;
        $req->_datas = $vars;
        return $req;
    }

    public function input ( )
    {
        return $this->environment_input();
    }
}
