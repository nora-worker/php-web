<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Web;

use Nora\Core\Module;

/**
 * WEBモジュール
 */
class Facade implements Module\ModuleIF
{
    use Module\Modulable;

    protected function initModuleImpl( )
    {
        // ウェブアプリを使用可能にする
        $this->setHelper([
            'Front' => [
                'scope',
                function ($scope) 
                {
                    $front = new Front();
                    $front->setScope($scope->newScope());
                    return $front;
                }
            ]
        ]);
    }
}
