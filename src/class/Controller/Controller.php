<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Web\Controller;

use Nora\Core\Component\Component;
use Nora\Core\Util\DocComment;

/**
 * WEBコントローラ
 */
class Controller extends Component
{
    static public function buildClosure($scope, $method, $injections = null)
    {
        $class = get_called_class();

        $ctrl = new $class( );
        $ctrl->setScope($scope);
        $injections[] = [$ctrl, $method];

        return $scope->makeClosure(
            $injections
        );
    }

    protected function initComponentImpl ( )
    {
        $this->initController( );
    }

    protected function initController ( )
    {
        $this->initControllerImpl( );
    }

    protected function initControllerImpl( )
    {
    }


    public function execute ($method)
    {
        $rc = new \ReflectionMethod(get_class($this).'::'.$method);
        $dc = DocComment::parse($rc->getDocComment());

        $injections = [];
        foreach($dc->gets('inject') as $name)
        {
            $injections[] = $name;
        }

        $closure = $this->buildClosure(
            $this->scope(),
            $method,
            $injections
        );

        return call_user_func($closure);
    }

}
