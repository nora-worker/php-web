<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Web;

use Nora\Core\Module\Module;

/**
 * WEBモジュール
 */
class Route
{
    public $pattern = '';

    protected $_delimiter = '/';
    private $_pattern;
    private $_spec = [];
    private $_matched_params = [];
    private $_matched_uri = false;
    private $_methods = null;

    private $_regex_delimiter = '/';
    private $_regex           = null;
    private $_keys            = [];

    /**
     * Create Route
     *
     * @param string $delimiter
     * @param string $pattern
     * @param array $params
     */
    public function __construct ( $pattern, $spec = [] )
    {
        $this->pattern = $pattern;

        if (false !== $p = strpos($pattern, ' '))
        {
            $this->_methods = array_map('trim', explode("|", substr($pattern, 0, $p)));
            $pattern = substr($pattern, $p+1);
        }
        $this->_pattern = $pattern;
        $this->_spec    = $spec;
    }

    /**
     * Compile Pattern To Regex
     *
     * @return array(string $regex, array $keys)
     */
    public function compile ( )
    {
        if ($this->_regex === null)
        {
            $this->_keys = [];

            $pattern = str_replace(['.', ')', '*'], ['\.', ')?', '.*?'], $this->_pattern);
            $pattern = str_replace($this->_regex_delimiter, '\\'.$this->_regex_delimiter, $pattern);

            $safe_d = preg_quote($this->_delimiter, $this->_regex_delimiter);

            $regex_for_pattern = '%2$s\{([\w]+)(\:([^%1$s]*))?\}%2$s';
            $regex_for_pattern = sprintf($regex_for_pattern,
                $safe_d,
                $this->_regex_delimiter
            );

            $idx = [];
            $callback = function($m) use ($safe_d, &$idx){
                $idx[$m[1]] = null;
                if (isset($m[3])) return sprintf('(?<%s>%s)', $m[1], $m[3]);
                return sprintf('(?<%s>[^%s]+)', $m[1], $safe_d);
            };

            $this->_regex = preg_replace_callback($regex_for_pattern, $callback, $pattern);
            $this->_keys = array_keys($idx);
        }
        return [$this->_regex, $this->_keys];
    }

    /**
     * Check wheter
     *
     * @param string $path
     * @param array &$matched_params
     * @return bool
     */
    public function match ($uri, $method, &$matched = null)
    {
        $this->_matched_params = [];
        $this->_matched_uri = (empty($method) ? '': "$method ").$uri;

        if (is_array($this->_methods)) {
            if(empty(array_intersect([$method], $this->_methods)))
            {
                return false;
            }
        }

        $matched_params    = [];
        list($regex,$keys) = $this->compile( );

        $regex = sprintf('%1$s^%2$s$%1$si', $this->_regex_delimiter, $regex);


        if (!preg_match($regex, $uri, $m, PREG_OFFSET_CAPTURE))
        {
            return false;
        }

        foreach ( $keys as $k )
        {
            if (isset($m[$k]) && !empty($m[$k][0])) {
                $matched_params[$k] = $m[$k][0];
            }
        }

        $matched = $matched_params;
        return true;
    }

    public function getSpec( )
    {
        return $this->_spec;
    }

    public function getMatchedUri( )
    {
        return $this->_matched_uri;
    }

    public function __tostring( )
    {
        list($regex, $keys) = $this->compile( );
        return sprintf('URI: %02$s REGEX: %01$s ', (string) $regex, $this->_matched_uri);
    }

}

