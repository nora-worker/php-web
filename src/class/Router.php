<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\Web;

use Nora\Core\Component;
use Nora\Core\Util\DocComment;

/**
 * WEBモジュール
 */
class Router
{
    use Component\Componentable;

    private $_routes = [];
    private $_names = [];

    public function initComponentImpl( )
    {
        $this->_routes = [];
        $this->_names = [];
    }

    public function addRoute(Route $route)
    {
        $this->_routes[spl_object_hash($route)] = $route;
        return $this;
    }

    public function addController ($class, $path = null, &$paths = [])
    {
        $paths = [];

        foreach (DocComment::parseMethods($class) as $m=>$dc)
        {
            if ($dc->has('route'))
            {
                $injections = [];


                foreach($dc->gets('inject') as $v)
                {
                    $injections[] = $v;
                }

                foreach($dc->gets('route') as $v)
                {
                    if ($v{0} !== '/')
                    {
                        list($method, $url) = explode(' ', $v, 2);
                        $method .= ' ';
                    }
                    else
                    {
                        $method = '';
                        $url = $v;
                    }

                    $route = $method.(rtrim(
                        $path === null || $path === '/' ? $url: $path.(
                            $url === '/' ? '': $url
                        ))
                    );

                    $this->logDebug([
                        'message' => 'ルート',
                        'route' => $route
                    ]);

                    $this->map($route, 'bind', [
                        'injections' => $injections,
                        'class'      => $class,
                        'method'     => $m->getName(),
                        'mask'       => $path
                    ]);

                    if ($dc->has('name')) {
                        $this->logDebug([
                            'name' => $dc->get('name'),
                            'route' => $route
                        ]);
                        $this->setName($dc->get('name'), $route);
                    }
                }
            }
        }
        return $this;
    }

    public function map($path)
    {
        $spec = func_get_args();
        array_shift($spec);

        return $this->addRoute(new Route($path, $spec));
    }

    public function route($path, $method = null)
    {
        while($route = current($this->_routes))
        {
            if ( $route->match($path, $method, $m) )
            {
                $placeholder = is_null($m) ? []: $m;
                $spec = $route->getSpec();

                return [
                    'placeholder' => $placeholder,
                    'spec' => $spec
                ];
            }

            next($this->_routes);
        }
        return false;
    }

    public function next( )
    {
        next($this->_routes);
        return $this;
    }

    public function rewind( )
    {
        reset($this->_routes);
        return $this;
    }

    public function setName($name, $path)
    {
        $this->_names[$name] = $path;
    }

    public function getNamePath($name, &$method = null)
    {
        $txt = $this->_names[$name];
        if (false !== $p = strpos($txt, ' '))
        {
            $method = substr($txt, 0, $p);
            $path = substr($txt, $p+1);
        }
        else
        {
            $method = 'GET';
        }
        return $path;
    }

    public function status ( )
    {
        $patterns = [];
        foreach ($this->_routes as $route)
        {
            $patterns[] = $route->pattern;
        }

        $names = [];
        foreach($this->_names as $k=>$v)
        {
            $names[$k] = $v;
        }

        return [
            'pattern' => $patterns,
            'names' => $names
        ];
    }

}
